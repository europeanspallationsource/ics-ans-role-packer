import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_packer_version(Command):
    cmd = Command('/usr/local/bin/packer version')
    assert 'Packer v1.1.0' in cmd.stdout.strip()

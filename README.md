ics-ans-role-packer
===================

Ansible role to install packer (https://www.packer.io).

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

```yaml
packer_version: 1.0.2
packer_url: https://releases.hashicorp.com/packer/{{packer_version}}/packer_{{packer_version}}_linux_amd64.zip
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-packer
```

License
-------

BSD 2-clause
